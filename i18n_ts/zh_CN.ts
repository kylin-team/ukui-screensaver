<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="zh_CN">
<context>
    <name>BioDeviceView</name>
    <message>
        <location filename="../src/bioAuthentication/biodeviceview.cpp" line="207"/>
        <source>password login</source>
        <translation>密码</translation>
    </message>
    <message>
        <location filename="../src/bioAuthentication/biodeviceview.cpp" line="213"/>
        <source>fingerprint</source>
        <translation>指纹</translation>
    </message>
    <message>
        <location filename="../src/bioAuthentication/biodeviceview.cpp" line="216"/>
        <source>fingerevin</source>
        <translation>指静脉</translation>
    </message>
    <message>
        <location filename="../src/bioAuthentication/biodeviceview.cpp" line="219"/>
        <source>iris</source>
        <translation>虹膜</translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="../src/mainwindow.ui" line="14"/>
        <source>MainWindow</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.ui" line="124"/>
        <source>Avatar</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.ui" line="186"/>
        <source>Switch User</source>
        <translation>切换用户</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.ui" line="209"/>
        <location filename="../src/mainwindow.ui" line="222"/>
        <source>TextLabel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.ui" line="72"/>
        <source>Username</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.ui" line="111"/>
        <source>Has Logged In</source>
        <translation>已登录</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.ui" line="98"/>
        <source>Unlock</source>
        <translation>解锁</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="421"/>
        <source>Password Incorrect</source>
        <translation>密码错误</translation>
    </message>
</context>
<context>
    <name>main</name>
    <message>
        <location filename="../src/main.cpp" line="33"/>
        <source>Dialog for the ukui ScreenSaver.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/main.cpp" line="38"/>
        <source>lock the screen immediately</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/main.cpp" line="40"/>
        <source>activated by session idle signal</source>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>
